const express = require('express')
const app = express()
const food = require('./models/food')

// log http request and response status
const morgan = require('morgan')
app.use(morgan('dev'))

// middleware to change req.method to the value of _method query param
const overrideMethod = (req, res, next) => {
  const methods = ['patch', 'put', 'delete']
  const methodAsked = req.query._method && req.query._method.toLowerCase()

  if (methodAsked && methods.find(m => m === methodAsked)) {
    req.originalMethod = req.originalMethod || req.method
    req.method = methodAsked
  }
  next()
}

app.use(overrideMethod)

// middleware to fill req.body
// parse application/json
app.use(express.json())

// cors
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Expose-Headers', 'Location, Origin, X-Requested-With, Content-Type, Accept')
  res.header('Access-Control-Allow-Headers', 'Location, Origin, X-Requested-With, Content-Type, Accept')
  res.header('Access-Control-Allow-Methods', 'DELETE, GET, POST, PUT, HEAD')
  next()
})

app.get('/food', (req, res) => {
  food.getAll().asCallback((err, list) => {
    if (err) return res.status(500).send('error, see server console')
    res.json(list)
  })
})

app.delete('/food/:id', (req, res) => {
  food.delete(req.params.id).asCallback((err, n) => {
    if (err) return res.send(err)
    // food not found
    if (n === 0) return res.status(404).send('no such food')
    res.type('json').status(200).send('food deleted')
  })
})
// food update
app.put('/food/:id', (req, res) => {
  const updatedFood = food.sanitize(req.body)
  food.update(req.params.id, updatedFood).asCallback((err, n) => {
    if (err) return res.status(500).send(err)
    if (n === 0) return res.status(404).send('no such food')
    res.type('json').status(200).send('food updated')
  })
})

app.get('/food/:id', (req, res) => {
  const foodId = req.params.id
  food.getOne(foodId).asCallback((err, f) => {
    if (err) return res.type('json').status(500).send('"error, see server console"')
    if (!f) return res.type('json').status(404).send('"no such food"')
    res.json(f)
  })
})

app.post('/food', (req, res) => {
  const newFood = food.sanitize(req.body)
  food.add(newFood).asCallback((err, id) => {
    if (err) return res.status(500).send('error, see server console')
    res
      .header('Location', `/food/${id[0]}`)
      .type('json')
      .status(201)
      .send('food created')
  })
})

const server = app.listen(2999, () => {
  const host = server.address().address
  const port = server.address().port

  console.log('Nutritool REST API listening at http://%s:%s', host, port)
})
