const environment = process.env.NODE_ENV || 'development'
const config = require('../knexfile.js')[environment]
const knex = require('knex')(config)

console.log(config)
function food () {
  return knex('food')
}

food.getAll = () => food().select()

food.getOne = function getOne (id) {
  return food().select().where('id', parseInt(id)).first()
}

food.add = (f) => food().insert(f, 'id')

food.update = (id, newValues) => food().where('id', parseInt(id)).update(newValues)

food.delete = (id) => food().where('id', parseInt(id)).delete()

food.nutrients = ['proteins', 'carbohydrates', 'lipids', 'weight' ]
food.properties = food.nutrients.concat(['name', 'description'])

// from an object, keeps only part matching key in food.nutrients
// returns a new object
food.sanitize = (f) =>
  Object.keys(f)
  .filter(prop => food.properties.includes(prop))
  .reduce((acc, prop) => { acc[prop] = f[prop]; return acc }, {})

module.exports = food
